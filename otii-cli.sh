#!/bin/sh

OTII=$(ls /dev/serial/by-id/usb-Qoitech*)

#echo echo $@ '>' $OTII

stty -F $OTII 115200 raw -echo
exec 3<$OTII
cat <&3 &
PID=$!

echo $@ > $OTII

sleep 0.1
kill $PID
wait $PID 2>/dev/null
exec 3<&-
