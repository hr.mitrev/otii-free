# otii-free

[A script](https://unix.stackexchange.com/a/372096/327778) to control [Qoitech Otii Arc](https://www.qoitech.com/otii) from the Command Line Interface

## Reverse Engineered Commands

getuid <br />
getname <br />
getsupplylimits <br />
getcaltime <br />
getmain <br />
getmainvolt <br />
getreqrange <br />
getexpvolt <br />
getuartbaud <br />
getadcres <br />
getoclimits <br />
get4wire <br />
getcalstatus <br />
get5v <br />
getgpo1 <br />
getgpo2 <br />
getgpi1 <br />
getgpi2 <br />
getsrccurlimit <br />
getmaxcmdlen <br />
getchgain [ac / ad / at / i1 / i2 / in / mc / md / mr / mv / rx / sn] <br />

calibrate <br />

mainvolt &lt;value mv&gt; <br />
commit <br />
main [on / off] <br />
srccurlimit [on / off] <br />
gpo1 [on / off] <br />
gpo2 [on / off] <br />
led [0 / 1 / on / off] <br />
led &lt;value ms on&gt;: &lt;value ms off&gt; <br />
start <br />
stop <br />

## A Word to Qoitech

The tool you are selling is great. However, charging licensing fees for basic scripting isn't. 

It's okay to sell advanced software, but keeping the low level interface a secret isn't very well tolerated by us - the Developers.

Cheers!
